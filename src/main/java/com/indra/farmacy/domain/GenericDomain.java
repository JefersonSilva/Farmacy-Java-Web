package com.indra.farmacy.domain;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@SuppressWarnings("serial")
@MappedSuperclass // Anotação para dizer que esta classe não correspomnde a uma tabela, mas será
					// usada por outros para gerar tabelas.
public class GenericDomain implements Serializable {

	@Id // dizer que esse é uma chave primária
	@GeneratedValue(strategy = GenerationType.SEQUENCE) // controle automático do id pelo banco
	private Long primaryKeyCod;

	public Long getPrimaryKeyCod() {
		return primaryKeyCod;
	}

	public void setPrimaryKeyCod(Long primaryKeyCod) {
		this.primaryKeyCod = primaryKeyCod;
	}

}
