package com.indra.farmacy.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
public class Cliente extends GenericDomain {
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dadaDoCadastro;
	@Column(nullable = false)
	private Boolean bloqueado;
	@OneToOne
	@JoinColumn(nullable=false)
	private Pessoa pessoa;

	public Date getDadaDoCadastro() {
		return dadaDoCadastro;
	}

	public void setDadaDoCadastro(Date dadaDoCadastro) {
		this.dadaDoCadastro = dadaDoCadastro;
	}

	public Boolean getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(Boolean bloqueado) {
		bloqueado = bloqueado;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

}
