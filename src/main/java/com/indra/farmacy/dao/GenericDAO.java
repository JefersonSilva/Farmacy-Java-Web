package com.indra.farmacy.dao;

import org.hibernate.Session;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.indra.farmacy.util.HibernateUtil;

public class GenericDAO<Entidade> {
	private Class<Entidade> classe;

	@SuppressWarnings("unchecked")
	public GenericDAO() {
		this.classe = (Class<Entidade>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	public void salvar(Entidade entidade) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.save(entidade);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			throw e;
		} finally {
			session.close();
		}

	}

	public void excluir(Entidade entidade) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.delete(entidade);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			throw e;
		} finally {
			session.close();
		}

	}

	public void editar(Entidade entidade) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.update(entidade);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			throw e;
		} finally {
			session.close();
		}

	}

	public List<Entidade> listar() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Entidade> criteria = builder.createQuery(classe);
			Root<Entidade> root = criteria.from(classe);
			criteria.select(root);
			List<Entidade> resultado = session.createQuery(criteria).getResultList();
			return resultado;
		} catch (RuntimeException e) {
			throw e;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public Entidade busca(Long cod) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Entidade> criteria = builder.createQuery(classe);
			Root<Entidade> root = criteria.from(classe);
			criteria.select(root);
			criteria.where(builder.equal(root.get("primaryKeyCod"), cod));
			Entidade resultado = session.createQuery(criteria).getSingleResult();
			return resultado;
		} catch (RuntimeException e) {
			throw e;
		} finally {
			session.close();
		}
	}

}
