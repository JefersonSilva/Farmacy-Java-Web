package com.indra.farmacy.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.indra.farmacy.domain.Estado;

public class EstadoDAOTest {
	@Test
	@Ignore
	public void salvar() {
		Estado estado = new Estado();
		estado.setNome("Paraiba");
		estado.setSigla("PB");
		EstadoDAO dao = new EstadoDAO();
		dao.salvar(estado);
		Estado estado2 = new Estado();
		estado.setNome("Pernambuco");
		estado.setSigla("PE");
		EstadoDAO dao2 = new EstadoDAO();
		dao.salvar(estado);
	}

	@Test
	@Ignore
	public void listar() {
		EstadoDAO dao = new EstadoDAO();
		List<Estado> estados = dao.listar();
		for (Estado estado : estados) {
			System.out.println(estado.getPrimaryKeyCod());
		}
		Assert.assertTrue(estados.size() == 2);
	}
	//@Ignore
	@Test
	public void buscar() {
		Long long1 = 15L;
		EstadoDAO dao = new EstadoDAO();
		System.out.println(dao.busca(long1).getNome());

	}
}
