package com.indra.farmacy.dao;

import org.junit.Test;

import com.indra.farmacy.domain.Cidade;
import com.indra.farmacy.domain.Estado;

public class CidadeDAOTest {

	@Test
	public void salvar() {
		Cidade cidade = new Cidade();
		cidade.setNome("Mamanguape");
		
		EstadoDAO estadoDao = new EstadoDAO();
		Estado estado = estadoDao.busca(15L);
		cidade.setEstado(estado);
		
		CidadeDAO cidadeDAO = new CidadeDAO();
		cidadeDAO.salvar(cidade);

	}

}
